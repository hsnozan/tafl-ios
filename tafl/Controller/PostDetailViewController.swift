//
//  PostDetailViewController.swift
//  deneme
//
//  Created by Ozan Al on 18.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import Kingfisher
import MXParallaxHeader

class PostDetailViewController: UIViewController {
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postDetail: UILabel!
    
    var postDetailItem: PostListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        let url = URL(string: postDetailItem?.imageUrl ?? "")
        postImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "first"))
        postTitle.text = postDetailItem?.title
        postDetail.text = postDetailItem?.postDetail
    }

}
