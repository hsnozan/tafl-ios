//
//  TeamPlayersViewController.swift
//  tafl
//
//  Created by Ozan Al on 25.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import Kingfisher

class TeamPlayersViewController: UIViewController {

    @IBOutlet weak var teamLogoImage: UIImageView!
    @IBOutlet weak var coachesTableView: UITableView!
    @IBOutlet weak var playersTableView: UITableView!
    var teamPlayers: NSArray?
    var teamCoaches: NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        coachesTableView.delegate = self
        coachesTableView.dataSource = self
        
        playersTableView.delegate = self
        playersTableView.dataSource = self
        
        setUpView()
    }
    
    func setUpView() {
        teamLogoImage.layer.cornerRadius = 35
        teamLogoImage.clipsToBounds = true
        var frame: CGRect = self.playersTableView.frame
        frame.size.height = self.playersTableView.contentSize.height
        self.playersTableView.frame = frame
        self.playersTableView.register(UINib(nibName: "TeamPlayerTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamPlayerTableViewCell")
        self.coachesTableView.register(UINib(nibName: "TeamCoachesTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamCoachesTableViewCell")
    }

}

extension TeamPlayersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if tableView == self.coachesTableView {
            count = 1
        }
        
        if tableView == self.playersTableView {
            count = teamPlayers?.count
        }
        
        return count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.playersTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamPlayerTableViewCell") as! TeamPlayerTableViewCell
            let cellModel = teamPlayers?[indexPath.row] as? NSDictionary
            let playerAge = cellModel?.value(forKey: "playerAge") as! Int
            let playerWeight = cellModel?.value(forKey: "playerWeight") as! Int
            let playerPosition = cellModel?.value(forKey: "playerPosition") as! String
            let playerImageUrlString = cellModel?.value(forKey: "playerImageUrl") as! String
            let url = URL(string: playerImageUrlString)
            cell.playerAge.text = "Yaş: " + "\(playerAge)"
            cell.playerWeight.text = "Kilo: " + "\(playerWeight)"
            cell.playerPosition.text = "Mevki: " + "\(playerPosition)"
            cell.playerName.text = cellModel?.value(forKey: "playerName") as? String
            cell.playerNumber.text = cellModel?.value(forKey: "playerNumber") as? String
            cell.playerImage.kf.setImage(with: url)
            
            let radius = cell.contentView.layer.cornerRadius
            cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
            cell.contentView.layer.masksToBounds = true
            cell.selectionStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCoachesTableViewCell") as! TeamCoachesTableViewCell
            return cell
        }
    }
}
