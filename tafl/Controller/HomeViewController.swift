//
//  FirstViewController.swift
//  deneme
//
//  Created by Ozan Al on 16.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import FirebaseDatabase

class HomeViewController: UIViewController {
    
    @IBOutlet weak var homeTableView: UITableView!
    var ref: DatabaseReference!
    var tableData = [PostListModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setDatabaseReference()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPostDetailVC" {
            if let indexPath = homeTableView.indexPathForSelectedRow {
                if let postDetailVC = segue.destination as? PostDetailViewController {
                    postDetailVC.postDetailItem = tableData[indexPath.row]
                }
            }
        }
    }

    func setDatabaseReference() {
        ref = Database.database().reference(fromURL: "https://tafl-b22b0.firebaseio.com/postList/postListItem")
        ref.observe(.value) { (snapshot) in
            var newTableData: [PostListModel] = []
            
            for datasnapshot in snapshot.children {
                if let data = datasnapshot as? DataSnapshot {
                    let postItem = data.value as? NSDictionary
                    let title = self.getFromPostDetail(valueText: "title", postItem: postItem!)
                    let imageUrl = self.getFromPostDetail(valueText: "imageUrl", postItem: postItem!)
                    let postDetail = self.getFromPostDetail(valueText: "postDetail", postItem: postItem!)
                    let postModel = PostListModel(imageUrl: imageUrl, postDetail: postDetail, title: title)
                    newTableData.append(postModel)
                }
            }
            
            self.tableData = newTableData.reversed()
            self.homeTableView.reloadData()
        }
    }
    
    func getFromPostDetail(valueText: String, postItem: NSDictionary) -> String {
        return (postItem[valueText] as? String)!
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeTableView") as! HomeTableViewCell
        let cellPost = tableData[indexPath.row]
        let radius = cell.contentView.layer.cornerRadius
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
        cell.updateView(postlistModel: cellPost)
        cell.contentView.layer.masksToBounds = true
        cell.selectionStyle = .none
        return cell
    }
    
}

