//
//  SecondViewController.swift
//  deneme
//
//  Created by Ozan Al on 16.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit
import FirebaseDatabase

class LeaguesViewController: UIViewController {

    @IBOutlet weak var leagueTableView: UITableView!
    var ref: DatabaseReference!
    var tableData = [LeagueListModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setDatabaseRef()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTeamTableVC" {
            if let indexPath = leagueTableView.indexPathForSelectedRow {
                if let teamTableVC = segue.destination as? TeamsViewController {
                    teamTableVC.leagueModel = tableData[indexPath.row]
                }
            }
        }
    }

    func setDatabaseRef() {
        ref = Database.database().reference(fromURL: "https://tafl-b22b0.firebaseio.com/teamList/teamListItem")
        ref.observe(.value) { (snapshot) in
            var newTableData: [LeagueListModel] = []
            for datasnapshot in snapshot.children {
                if let data = datasnapshot as? DataSnapshot {
                    let leagueItem = data.value as? NSDictionary
                    let leagueName = leagueItem?["leagueName"] as? String
                    
                    if let teamsList = leagueItem?["teamsList"] as? NSArray {
                        let leagueModel = LeagueListModel(leagueName: leagueName ?? "", teamsList: teamsList)
                        newTableData.append(leagueModel)
                    }
                }
            }
            
            self.tableData = newTableData
            self.leagueTableView.reloadData()
        }
    }
}

extension LeaguesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leagueTableView") as! LeagueTableViewCell
        let cellLeagueModel = tableData[indexPath.row]
        cell.updateView(leagueListModel: cellLeagueModel)
        cell.contentView.layer.masksToBounds = true
        cell.selectionStyle = .none
        let radius = cell.contentView.layer.cornerRadius
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
        return cell
    }
    
    
}

