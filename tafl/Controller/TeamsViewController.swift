//
//  TeamsViewController.swift
//  tafl
//
//  Created by Ozan Al on 25.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

class TeamsViewController: UIViewController {
    
    var leagueModel: LeagueListModel!
    var leagueSortedArray: NSArray!
    @IBOutlet weak var teamTableView: UITableView!
    var teamPlayers: NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "teamName", ascending: true, selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        leagueSortedArray = leagueModel.teamsList.sortedArray(using: [descriptor]) as NSArray
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toTeamDetailVC" {
            if let indexPath = teamTableView.indexPathForSelectedRow {
                if let teamTableVC = segue.destination as? TeamPlayersViewController {
                    let cellModel = leagueSortedArray[indexPath.row] as? NSDictionary
                    teamPlayers = cellModel?.value(forKey: "teamPlayers") as? NSArray ?? []
                    teamTableVC.teamPlayers = teamPlayers
                }
            }
        }
    }
}

extension TeamsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leagueSortedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teamTableView") as! TeamTableViewCell
        let cellModel = leagueSortedArray[indexPath.row] as? NSDictionary
        let teamNameText = cellModel?.value(forKey: "teamName") as? String
        let teamLogoUrlText = cellModel?.value(forKey: "teamLogoUrl") as? String
        let teamPlayersList = cellModel?.value(forKey: "teamPlayers") as? NSArray ?? []
        let radius = cell.contentView.layer.cornerRadius
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
        cell.updateView(teamName: teamNameText, teamLogoUrl: teamLogoUrlText, teamPlayers: teamPlayersList)
        cell.contentView.layer.masksToBounds = true
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
