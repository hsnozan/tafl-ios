//
//  LeagueListModel.swift
//  tafl
//
//  Created by Ozan Al on 24.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation

struct LeagueListModel {
    
    var leagueName: String?
    var teamsList: NSArray
    
    init(leagueName: String, teamsList: NSArray) {
        self.leagueName = leagueName
        self.teamsList = teamsList
    }
}
