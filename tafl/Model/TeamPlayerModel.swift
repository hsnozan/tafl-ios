//
//  TeamPlayerModel.swift
//  tafl
//
//  Created by Ozan Al on 24.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation

struct TeamPlayerModel {
    
    var playerAge: Int?
    var playerImageUrl: String?
    var playerName: String?
    var playerNumber: String?
    var playerPosition: String?
    var playerWeight: String?
}
