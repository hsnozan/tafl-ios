//
//  TeamListModel.swift
//  tafl
//
//  Created by Ozan Al on 24.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation

struct TeamListModel {
    
    var teamLogoUrl: String?
    var teamName: String?
    var teamPlayers: [TeamPlayerModel]
}
