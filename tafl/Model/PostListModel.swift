//
//  PostListModel.swift
//  deneme
//
//  Created by Ozan Al on 16.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation

struct PostListModel {
    
    var imageUrl: String?
    var postDetail: String?
    var title: String?
    
    init(imageUrl: String, postDetail: String, title: String) {
        self.imageUrl = imageUrl
        self.postDetail = postDetail
        self.title = title
    }
}
