//
//  LeagueTableViewCell.swift
//  tafl
//
//  Created by Ozan Al on 24.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

class LeagueTableViewCell: UITableViewCell {

    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layout()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func layout() {
        self.contentView.backgroundColor = UIColor.clear
        let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 25.0)
        cellView.layer.masksToBounds = false
        cellView.layer.shadowRadius = 30
        cellView.layer.shadowColor = UIColor.black.cgColor
        cellView.layer.shadowOffset = CGSize(width: 0, height: 10)
        cellView.layer.shadowOpacity = 0.10
        cellView.layer.shadowPath = shadowPath.cgPath
        cellView.layer.cornerRadius = 15
        cellView.clipsToBounds = true
    }
    
    func updateView(leagueListModel: LeagueListModel) {
        leagueName.text = leagueListModel.leagueName
    }

}
