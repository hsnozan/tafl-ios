//
//  TeamPlayerTableViewCell.swift
//  tafl
//
//  Created by Ozan Al on 26.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import UIKit

class TeamPlayerTableViewCell: UITableViewCell {

    @IBOutlet weak var playerNumber: UILabel!
    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerAge: UILabel!
    @IBOutlet weak var playerPosition: UILabel!
    @IBOutlet weak var playerHeight: UILabel!
    @IBOutlet weak var playerWeight: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
