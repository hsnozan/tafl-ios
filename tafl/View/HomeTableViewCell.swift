//
//  HomeTableViewCell.swift
//  deneme
//
//  Created by Ozan Al on 16.04.2019.
//  Copyright © 2019 Ozan Al. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var postLabel: UILabel!
    let padding = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layout()
    }
    
    func updateView(postlistModel: PostListModel) {
        let url = URL(string: postlistModel.imageUrl ?? "")
        postImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "second"))
        postLabel.text = postlistModel.title
        postLabel.frame.inset(by: self.padding)
    }
    
    fileprivate func layout() {
        self.contentView.backgroundColor = UIColor.clear
        let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: 25.0)
        cellView.layer.masksToBounds = false
        cellView.layer.shadowRadius = 30
        cellView.layer.shadowColor = UIColor.black.cgColor
        cellView.layer.shadowOffset = CGSize(width: 0, height: 10)
        cellView.layer.shadowOpacity = 0.10
        cellView.layer.shadowPath = shadowPath.cgPath
        cellView.layer.cornerRadius = 15
        cellView.clipsToBounds = true
    }
}
